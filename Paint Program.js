
//helper functions
function relativePos(event, element){
	var rect = element.getBoundingClientRect();
	return {x: Math.floor(event.clientX - rect.left),
			y: Math.floor(event.clientY - rect.top)};
}

function trackDrag(onMove, onEnd){
	function end(event){
		removeEventListener("mousemove", onMove);
		removeEventListener("mouseup", end);
		if(onEnd){
			onEnd(event);
		}
	}
	addEventListener("mousemove", onMove);
	addEventListener("mouseup", end);
}

function loadImageURL(cx, url){
	var image = document.createElement("img");
	image.addEventListener("load", function(){
		var color = cx.fillStyle, size = cx.lineWidth;
		cx.canvas.width = image.width;
		cx.canvas.height = image.height;
		cx.drawImage(image, 0, 0);
		cx.fillStyle = color;
		cx.strokeStyle = color;
		cx.lineWidth = size;
	});
	image.src = url;
}

function randomPointInRadius(radius){
	for(;;){
		var x = Math.random() * 2 - 1;
		var y = Math.random() * 2 - 1;
		if(x * x + y * y <= 1){
			return {x: x * radius, y: y * radius}
		}
	}
}

function pixelAt(cx, x, y){
	var data = cx.getImageData(x, y, 1, 1);
	return data;
}

//Initialization functions
function elt(name, attributes){
	var node = document.createElement(name);
	if(attributes){
		for(var attr in attributes){
			if(attributes.hasOwnProperty(attr)){
				node.setAttribute(attr, attributes[attr]);
			}
		}
	}
	for(var i = 2; i < arguments.length; i++){
		var child = arguments[i];
		if(typeof child == "string"){
			child = document.createTextNode(child);
		}
		node.appendChild(child);
	}
	return node;
}

var controls = Object.create(null);

function createPaint(parent){
	var canvas = elt("canvas", {width: 500, height: 300});
	var cx = canvas.getContext("2d");
	var toolbar = elt("div", {class: "toolbar"});
	for(var name in controls){
		toolbar.appendChild(controls[name](cx));
	}
	
	var panel = elt("div", {class: "picturePanel"}, canvas);
	parent.appendChild(elt("div", null, panel, toolbar));
}

//Controls
var tools = Object.create(null);

controls.tool = function(cx){
	var select = elt("select");
	for(var name in tools){
		select.appendChild(elt("option", null, name));
	}
	
	cx.canvas.addEventListener("mousedown", function(event){
		if(event.which == 1){
			tools[select.value](event, cx);
			event.preventDefault();
		}
	});
	
	return elt("span", null, "Tool: ", select);
};

controls.color = function(cx){
	var input = elt("input", {type: "color"});
	input.addEventListener("change", function(){
		cx.fillStyle = input.value;
		cx.strokeStyle = input.value;
	});
	return elt("span", null, "Color: ", input);
};

controls.brushSize = function(cx){
	var select = elt("select");
	var sizes = [1, 2, 3, 5, 8, 12, 25, 35, 50, 75, 100];
	sizes.forEach(function(size){
		select.appendChild(elt("option", {value: size}, size + "pixels"));
	});
	select.addEventListener("change", function(){
		cx.lineWidth = select.value;
	});
	return elt("span", null, "Brush size: ", select);
};

controls.save = function(cx) {
	var link = elt("a", {href: "/"}, "Save");
	function update() {
		try {
			link.href = cx.canvas.toDataURL();
		}catch(e) {
			if (e instanceof SecurityError)
				link.href = "javascript:alert(" +
				JSON.stringify("Can't save: " + e.toString()) + ")";
		else
			throw e;
		}
	}
	link.addEventListener("mouseover", update);
	link.addEventListener("focus", update);
	return link;
};

controls.openFile = function(cx){
	var input = elt("input", {type: "file"});
	input.addEventListener("change", function(){
		if(input.files.length == 0) return;
		var reader = new FileReader();
		reader.addEventListener("load", function(){
			loadImageURL(input.files[0]);
		});
		reader.readAsDataUrl(input.files[0]);
	});
	return elt("div", null, "Open file: ", input);
};

controls.openURL = function(cx){
	var input = elt("input", {type: "text"});
	var form = elt("form", null, "Open URL: ", input, elt("button", {type: "submit"}, "load"));
	form.addEventListener("submit", function(event){
		event.preventDefault();
		loadImageURL(cx, input.value);
	});
	return form;
};

controls.clear = function(cx){
	var input = elt("input", {type: "reset"});
	var form = elt("form", null, "Clear Page: ", input);
	form.addEventListener("reset", function(){
		var width = cx.canvas.scrollWidth;
		var height = cx.canvas.scrollHeight;
		cx.clearRect(0, 0, width, height);
	});
	return form;
}

//tools
tools["Line"] = function(event, cx, onEnd){
	cx.lineCap = "round";
	
	var pos = relativePos(event, cx.canvas);
	trackDrag(function(event){
		cx.beginPath();
		cx.moveTo(pos.x, pos.y);
		pos = relativePos(event, cx.canvas);
		cx.lineTo(pos.x, pos.y);
		cx.stroke();
	}, onEnd);
};

tools["Erase"] = function(event, cx){
	cx.globalCompositeOperation = "destination-out";
	tools.Line(event, cx, function(){
		cx.globalCompositeOperation = "source-over";
	});
};

tools["Text"] = function(event, cx){
	var text = prompt("Text:", "");
	if(text){
		var pos = relativePos(event, cx.canvas);
		cx.font = Math.max(7, cx.lineWidth) + "px sans-serif";
		cx.fillText(text, pos.x, pos.y);
	}
};

tools["Spray"] = function(event, cx){
	var radius = cx.lineWidth / 2;
	var area = radius * radius * Math.PI;
	var dotsPerTick = Math.ceil(area / 30);
	
	var currentPos = relativePos(event, cx.canvas);
	var spray = setInterval(function(){
		for(var i = 0; i < dotsPerTick; i++){
			var offset = randomPointInRadius(radius);
			cx.fillRect(currentPos.x + offset.x, currentPos.y + offset.y, 1, 1);
		}
	}, 25);
	trackDrag(function(event){
		currentPos = relativePos(event, cx.canvas);
	}, function(){
		clearInterval(spray);
	});
};

tools["Rectangle"] = function(event, cx){
	var pos = relativePos(event, cx.canvas);
	var start = {x: event.pageX, y: event.pageY};
	var rect = document.createElement("div");
	rect.style.background = cx.fillStyle;
	rect.style.position = "absolute";
	document.body.appendChild(rect);
	function onMove(event){
		var end = {x: event.pageX, y: event.pageY};
		rect.style.left = Math.min(start.x, end.x) + "px";
		rect.style.top = Math.min(start.y, end.y) + "px";
		rect.style.width = ((start.x > end.x) ? start.x - end.x : end.x - start.x ) + "px";
		rect.style.height = ((start.y > end.y) ? start.y - end.y : end.y - start.y ) + "px";
	}
	function end(event){
		document.body.removeChild(rect);
		var end = relativePos(event, cx.canvas);
		cx.fillRect(pos.x, pos.y, end.x - pos.x, end.y - pos.y);
	}
	trackDrag(onMove, end);
};

tools["Color Picker"] = function(event, cx){
	var pos = relativePos(event, cx.canvas);
	try{
		var data = pixelAt(cx, pos.x, pos.y).data;
	}catch(error){
		alert("Can't use the color picker on images loaded by URL");
	}
	var rgb = "rgb(" + data[0] + ", " + data[1] + ", " + data[2] + " )";
	cx.fillStyle = rgb;
	cx.strokeStyle = rgb;
};

function Vector(x, y){
	this.x = x;
	this.y = y;
}

var checked;
var toCheck;

tools["Flood Fill"] = function(event, cx){
	var pos = relativePos(event, cx.canvas);
	var image =  cx.getImageData(0, 0, cx.canvas.width, cx.canvas.height).data;
	var current = (pos.x + pos.y * cx.canvas.width) * 4;
	var rgb = {r: image[current], g: image[current + 1], b: image[current + 2], a: image[current + 3]};
	checked = new Array(cx.canvas.width * cx.canvas.height * 4);
	toCheck = new Array();
	for(var i = 0; i < checked.length; i++){
		checked[i] = 0;
	}
	toCheck.push(new Vector(pos.x, pos.y));
	walk(rgb, cx, image);
};

function walk(color, cx, image){
	var next;
	if(toCheck.length > 0){
		next = toCheck.pop();
	}else{
		return;
	}
	cx.fillRect(next.x, next.y, 1, 1);
	checked[next.x + next.y * cx.canvas.width] = 1;
	
	if(look(next.x + 1, next.y, color, cx, image) 
		&& checked[(next.x + 1) + next.y * cx.canvas.width] == 0
		 && next.x + 1 < cx.canvas.width){
		toCheck.push(new Vector(next.x + 1, next.y));	
	}
	if(look(next.x - 1, next.y, color, cx, image) 
		&& checked[(next.x - 1) + next.y * cx.canvas.width] == 0
		 && next.x - 1 > 0){
		toCheck.push(new Vector(next.x - 1, next.y));	
	}
	if(look(next.x, next.y + 1, color, cx, image) 
		&& checked[next.x + (next.y + 1) * cx.canvas.width] == 0
		 && next.y + 1 < cx.canvas.height){
		toCheck.push(new Vector(next.x, next.y + 1));	
	}
	if(look(next.x, next.y - 1, color, cx, image) 
		&& checked[next.x + (next.y - 1) * cx.canvas.width] == 0
		 && next.y - 1 > 0){
		toCheck.push(new Vector(next.x, next.y - 1));	
	}
	walk(color, cx, image);
}

function look(x, y, color, cx, image){
	var current = (x + y * cx.canvas.width) * 4;
	if(image[current] == color.r &&
		image[current + 1] == color.g &&
		 image[current + 2] == color.b &&
		  image[current + 3] == color.a){
		return true;
	}else{
		return false;
	}
}






























